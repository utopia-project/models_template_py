# Template for utopya-based model implementations

A template from which to create your own [utopya]-based model repository:

The template sets up a default [utopya] project:

- A `models` folder including an adaptable demo model (using the [utopya model backend][utopya-backend-docs]).
- A Python *support package* that can be used for all the shared infrastructure between models (including a pre-populated `setup.py` and test infrastructure).
- Locations for custom model plots and tests, including defaults.
- Default configurations for Python-based utopya projects.

In addition, the template integrates a number of tools to make development and maintenance of the implemented models easier:

- Documentation build using [Sphinx][sphinx]
- [Pytest][pytest] environment with basic fixtures
- [Tox][tox] setup to test against different Python versions
- [pre-commit] configuration, including auto-formatting using [black][black]
- Basic [GitLab CI/CD][GitLab-CI-CD] configuration
- [GitLab Issue and MR templates][GitLab-templates]


**Questions or problems with this template?**
Please open an issue here in this repository.


[[_TOC_]]

## Instructions
To make setting up a new [utopya] project repository as easy as possible, the [cookiecutter](https://github.com/cookiecutter/cookiecutter#features) tool allows to clone a template project (this repository) and make the necessary adjustments on the way.
For instance, the `{{cookiecutter.project_slug}}` folder you see in the root of this repository will be named using the `project_slug` variable; the same holds for these kinds of variables *inside* any of the files in this project.

*Note:* This template is optimized for hosting the remote on a GitLab instance.
However, it can easily be adapted to GitHub or other hosters; see below.


### Step 1: Install `cookiecutter`
```bash
pip3 install cookiecutter
```


### Step 2: Create your project from the template

☝️**Before proceeding, read the rest of this step carefully!**

First, enter the directory in which your project repository should be created in.

To create your project repository from the template, call the following command:

```bash
cookiecutter https://gitlab.com/utopia-project/models_template_py.git
```

This will prompt for some input from your side.
Let's go through the prompts one by one.
They are sorted into the following categories:

#### Author and project information
The first set of prompts is about the author (you, probably) and the name of the project.
If you intend to include multiple models into this project, try to think of a name that describes all these models.

* `author`: your name or that of your team *(may contain spaces)*
* `author_email`: an e-mail address you can be reached by *(optional)*
* `project_name`: the readable name of this project *(may contain spaces)*
* `project_abbreviation`: an abbreviation of the project name, used where space is low. If your project name is already short (<8 characters), simply keep it.
* `project_description_short`: a one-sentence description of this project *(optional)*
* `project_slug` : A lower-case version of the project name, without spaces. This will become the name of the repo's root directory.
* `utopya_project_name`: How utopya will refer to your project.

#### Model name and Python package
This second set of prompts is about the Python infrastructure of your new project repository.
The template creates the folder structure for model implementations.
In addition, a Python support package is set up that can be used for all the *shared* model utilities – the models itself are *not* implemented in that package.

* `model_name`: The name of your (first) model in this project repository.
    * ⚠️ This needs to be different from the `project_name`.
    * ⚠️ Should not contain spaces.
* `package_name` : The name of the Python "utility" package, i.e. where code that is shared *across models* can be implemented in.
    * ⚠️ Should be different from `model_name` and `project_name` to avoid confusion.
    * ⚠️ Should not contain spaces and should follow Python naming conventions.
* `package_version`: The initial version of the package.

#### Git-related information
Entering this information is *optional*.
However, if you plan on hosting the repository somewhere anyway, filling this out will pre-populate some of the documentation entries – less work for future you.

* `gitlab_group`: The name of the GitLab group the repo is part of; for your personal repository, this will simply be your GitLab user name. *(optional)*
* `gitlab_repo_slug`: The slug of the repository's remote URL, i.e. the last segment of the `repo_url`. *(optional)*
* `repo_url`: The URL of the repository this project will be hosted at; should *not* be the clone URL. The default value will be composed of `gitlab_group` and `gitlab_repo_slug`. If your project will live on GitHub, you can adjust the URL here.

#### Let cookiecutter do its work
Now follow the prompts and enter the corresponding information.
In many cases, using the suggested default is a good choice.

Remember that you can always use `Ctrl + C` to stop cookiecutting and start over, e.g. when you have entered wrong information.

After this, cookiecutter will hopefully inform you that it has succeeded.
Your utopya project repository is now basically set up! 🎉

To see more options, call `cookiecutter --help`.
Also refer to the [cookiecutter documentation][cookiecutter_docs] for more information on the available configuration options in this step.


### Step 3: Test that it works
Enter the created directory; it will be named like the `project_slug` you entered above.

Now, **follow the instructions in the *created project's* README** to get everything up and running.
Once you have finished the setup, you should be able to run the dummy model that is part of this template:

```bash
utopya run YourModelName
```


### Step 4: Make adjustments
There are a bunch of adjustments you can now do on your repository to get ready for implementing your models.

#### Set up version control *(recommended)*
Your new project does not come with version control yet.
We should definitely change that:

```bash
git init
git remote add origin <THE-PROJECT-REPOSITORY-URL>
```

You probably want a remote host for your git repository.
This template project is specialized for hosting on **GitLab** instances and provides issue and MR templates as well as a CI/CD configuration.
If you want to host there, create a new project now and follow the steps there to connect it to your local repository.

At this point, you can already commit and push if you want, but you can also wait and carry out any further adjustments; it's completely up to you.

For hosting on other platforms, the procedure is basically the same.
In such a case, you can delete the `.gitlab` directory and the `.gitlab-ci.yml` file.

The template also includes some [pre-commit][pre-commit] hooks.
To install the corresponding git hooks, run:

```bash
pre-commit install
```


#### Adjust dependencies *(recommended)*
The `setup.py` file defines some dummy dependencies, which you should adjust.


#### Populate GitLab CI/CD (or delete it)
*Proceed only, if you know what this is and have at least a little bit of experience with it. Otherwise, skip this step.*

In case you want to use GitLab CI/CD for automated testing, the template project comes with a `.gitlab-ci.yml` to get you started.
If you don't plan on using it, you can safely delete that file.

The CI/CD as configured in the template does the following:

* Run static checks on the code, including code-formatting
* Run tests using different Python versions
* Build and deploy the documentation to GitLab Pages

In very broad strokes, you *may* need to make the following adjustments in `.gitlab-ci.yml` or in the GitLab Project settings:

* Check that the `PAGES_URL` variable is correct and GitLab Pages is activated in the project settings.
* Check that the GitLab Environments feature is enabled.
* Add or adapt the Python test jobs, if you want to support different Python versions (also adjust `tox.ini` accordingly).


### Step 5: Start working on your own models.
That's it.
You can now start developing and running your utopya models. 🎉

Again, refer to the *created project's* README for more information, e.g. on where to find the model implementation.
Other useful information can be found in the [utopya documentation][utopya-docs].


## Copyright & License
This project is free open source software, licensed under the [MIT License][MIT].
See [`LICENSE.txt`](LICENSE.txt) for the full text of the license.


<!-- Links -->

[utopya]: https://gitlab.com/utopia-project/utopya
[utopya-docs]: https://utopya.readthedocs.io/
[utopya-backend-docs]: https://utopya.readthedocs.io/en/latest/backend/overview.html

[MIT]: https://choosealicense.com/licenses/mit/
[cookiecutter_docs]: http://cookiecutter.readthedocs.io
[pytest]: https://pytest.org/
[black]: https://black.readthedocs.io/
[pre-commit]: https://pre-commit.com
[tox]: https://tox.wiki/
[sphinx]: https://www.sphinx-doc.org
[GitLab-CI-CD]: https://docs.gitlab.com/ee/ci/
[GitLab-templates]: https://docs.gitlab.com/ee/user/project/description_templates.html
