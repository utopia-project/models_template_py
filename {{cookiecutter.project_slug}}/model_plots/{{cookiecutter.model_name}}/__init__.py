"""
Model plots and custom operations for the
{{cookiecutter.model_name}} model.
"""

from .operations import *
from .plots import *
