"""Tests package import"""

import pytest


def test_import():
    """Tests whether package import works"""
    import {{cookiecutter.package_name}}
