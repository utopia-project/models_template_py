"""Tests the configuration sets for the {{cookiecutter.model_name}}"""

import pytest
from utopya.testtools import ModelTest

mtc = ModelTest("{{cookiecutter.model_name}}", test_file=__file__)
"""The ModelTest class instance used to spawn simulations"""

# Fixtures --------------------------------------------------------------------


# Tests -----------------------------------------------------------------------


def test_run_and_eval_cfgs():
    """Carries out all additional configurations that were specified alongside
    the default model configuration.

    This is done automatically for all run and eval configuration pairs that
    are located in subdirectories of the ``cfgs`` directory (at the same level
    as the default model configuration).
    If no run or eval configurations are given in the subdirectories, the
    respective defaults are used.

    See :py:meth:`~utopya.model.Model.run_and_eval_cfg_paths` for more info.
    """
    for cfg_name, cfg_paths in mtc.default_config_sets.items():
        print(f"\nRunning '{cfg_name}' example ...")

        mv, _ = mtc.create_run_load(
            from_cfg=cfg_paths.get("run"), parameter_space=dict(num_steps=10)
        )
        mv.pm.plot_from_cfg(plots_cfg=cfg_paths.get("eval"))

        print(f"Succeeded running and evaluating '{cfg_name}'.\n")
