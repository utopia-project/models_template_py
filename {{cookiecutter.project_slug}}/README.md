# {{cookiecutter.project_name}}

{{cookiecutter.project_description_short}}

This repository uses the [utopya] modelling framework for implementation of models, performing simulation runs, and evaluating their output.
For more information, visit the [Utopia Project website][utopia-project].

[Visit the project repository][repository].


## Setup
The following steps will help you to setup this models repository locally.
For further developing the models, also refer to [the developers section below](#for-developers).

1. Navigate to the folder where you would like to clone this repository to.
   If you already have a `Utopia` directory that contains other model projects, it's best to go there.
1. Clone this repository:

    ```bash
    git clone {{cookiecutter.repo_url}}
    ```

1. Enter the project directory:

    ```bash
    cd {{cookiecutter.project_slug}}
    ```

1. *(Optional)* If you want to use a virtual environment, create and/or enter it now.
1. Install the support package and its dependencies using [pip]:

    ```bash
    pip install -e .
    ```

1. Register the `{{cookiecutter.project_slug}}` project and the available models with [utopya]:

    ```bash
    utopya projects register . --with-models
    ```


## Use
To run a model, simply call the [utopya CLI][utopya-cli]:

```bash
utopya run {{cookiecutter.model_name}}
```

To run only the evaluation routine for a previous simulation run, call:

```bash
utopya eval {{cookiecutter.model_name}}
```

Refer to the [utopya documentation][utopya-docs] or the [Utopia webpage][utopia-project] for more information on how to run and evaluate models.


### Troubleshooting
* If you used a virtual environment during setup, make sure you have activated it before calling `utopya`.
* If you get an error that states, that the model was not executable, you may have to adjust permissions on the `run_model.py` file specified in the error message: Navigate to that folder and then call `chmod +x run_model.py` on it.

If there are further issues, feel free to raise an issue in the [utopya repository][utopya].


## For developers
If you plan on further developing the models in this project, some additional setup steps are recommended.

First of all, you should best install the package in editable mode and with some additional development-related dependencies (for tests and building of the documentation):

```bash
cd {{cookiecutter.project_slug}}
pip install -e .[dev]
```


### Project overview
The repository is structured as follows:

* [`models`](models) is where all *actual* model implementations live.
    * `models/{ModelName}/run_model.py` is the executable that invokes each model
    * `models/{ModelName}/impl/model.py` is where the actual model is implemented.
        * The most important methods are: `setup`, `perform_step`, and `write_data`. More information can be found in the [documentation of the `utopya_backend` package][utopya-backend-docs].
* [`model_plots`](model_plots) and [`model_tests`](model_tests) contain model-specific plot and test implementations.
* The [`cfg`](cfg)` directory contains project-level configuration files, i.e. configurations that apply to all models in this project.
* The [`.utopya-project.yml`](.utopya-project.yml) file denotes this repository as a [utopya] project and contains some metadata.
  If you make changes here, make sure to call `utopya register .` again.


### Pre-commit hooks
To automatically run [pre-commit][pre-commit] hooks, install the configured git hooks using

```bash
pre-commit install
```

These hooks run each time you make a commit, and help in keeping the project repository clean.
For instance, automated code formatting is applied using [black][black].
These hooks can be configured via the [`.pre-commit-config.yaml`](.pre-commit-config.yaml) file.


### Running tests
To run tests for the `{{cookiecutter.package_name}}` package, invoke [pytest][pytest]:

```bash
python -m pytest -v tests/ --cov={{cookiecutter.package_name}} --cov-report=term-missing
```

For model-specific tests, you can also use the utopya CLI, specifying the name of the model you would like to run tests for.

```bash
utopya test {{cookiecutter.model_name}}
```


### Building the documentation
```bash
cd doc
make doc
make linkcheck  # optional
make doctest    # optional
```

The documentation can then be found in [`doc/_build/html`](doc/_build/html/).

To automatically generate figures, set the `{{ cookiecutter.project_abbreviation.upper() }}_USE_TEST_OUTPUT_DIR` environment variable before invoking `make doc`.

```bash
export {{ cookiecutter.project_abbreviation.upper() }}_USE_TEST_OUTPUT_DIR=yes
```


## Copyright
```
{{cookiecutter.project_name}}
(c) {% now 'utc', '%Y' %}, {{cookiecutter.author}}

🚧 <insert license here>
```

### Copyright holders

- {{cookiecutter.author}}


[repository]: {{cookiecutter.repo_url}}
[utopya]: https://gitlab.com/utopia-project/utopya
[utopia-project]: https://utopia-project.org/
[utopya-docs]: https://utopya.readthedocs.io/
[utopya-backend-docs]: https://utopya.readthedocs.io/en/latest/backend/overview.html
[utopya-cli]: https://utopya.readthedocs.io/en/latest/cli/index.html
[pip]: https://pip.pypa.io/en/stable/
[pytest]: https://pytest.org/
[pre-commit]: https://pre-commit.com
[black]: https://github.com/psf/black
