"""
{{cookiecutter.package_name}}:

A support package for the model implementations that are part of the
{{cookiecutter.package_name}} project repository.

.. note::

    Models themselves are *not* implemented here.
"""

__version__ = "{{cookiecutter.package_version}}"
